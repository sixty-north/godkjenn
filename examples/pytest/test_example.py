import json

import pytest


@pytest.fixture(params=[b"1", b"2", b"3"])
def ids(request):
    return request.param


# Use the godkjenn fixture
def test_example(godkjenn, ids):
    # Calculate your new received data
    received = b"received data " + ids

    # Use the fixture's `verify()` method to compare it with the accepted data.
    godkjenn.verify(received, mime_type="application/octet-stream")


def test_json_data(godkjenn):
    data = {1: 2, 3: {4: 5, 6: 7}}
    godkjenn.verify(json.dumps(data).encode("utf-8"))
