import tempfile
from contextlib import contextmanager

from godkjenn.vaults.fs_vault import FSVault


@contextmanager
def temp_vault():
    # We can't use a fixture for this because it interferes with hypothesis...
    with tempfile.TemporaryDirectory() as vault_root:
        yield FSVault(vault_root)


OCTET_STREAM = "application/octet-stream"
