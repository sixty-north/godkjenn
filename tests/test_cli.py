from io import BytesIO
from unittest.mock import patch

import pytest
from exit_codes import ExitCode

from godkjenn.cli import main
from godkjenn.vaults.fs_vault import FSVault


from utils import OCTET_STREAM  # noqa: F401


@pytest.fixture
def context(tmp_path):
    root_path = tmp_path / ".godkjenn"
    vault = FSVault(root_path / "vault")

    def runner(args, stdin=b""):
        args = ["-C", str(tmp_path)] + list(args)
        output = BytesIO()
        with patch("sys.stdin", BytesIO(stdin)):
            with patch("sys.stdout", output):
                return_code = main(args, standalone_mode=False)
                output.seek(0)
                return return_code, output.read()

    return runner, vault


def test_accept_received(context):
    run, vault = context

    test_id = "test-id"
    rx = b"received"

    run(["receive", test_id, "-", OCTET_STREAM], stdin=rx)
    result, _ = run(["accept", test_id])

    assert result == ExitCode.OK

    result, output = run(["accepted", test_id, "-"])
    assert result == ExitCode.OK
    assert output == rx


def test_accept_with_no_existing_vault(context):
    run, vault = context
    assert run(["accept", "test-id-2"])[0] == ExitCode.OS_FILE


def test_accept_with_no_received(context):
    run, vault = context
    run(["receive", "text-id-1", "-", OCTET_STREAM], stdin=b"received")
    assert run(["accept", "test-id-2"])[0] == ExitCode.DATA_ERR


def test_accept_accepts_only_one(context):
    run, vault = context
    test_ids = [f"test-id-{i}" for i in range(10)]
    rx = b"received"
    for test_id in test_ids:
        run(["receive", test_id, "-", OCTET_STREAM], stdin=rx)

    run(["accept", test_ids[0]])

    result, output = run(["accepted", test_ids[0], "-"])
    assert output == rx

    for test_id in test_ids[1:]:
        result, _ = run(["accepted", test_id, "-"])
        assert result == ExitCode.DATA_ERR


def test_accept_all_accepts_all(context):
    run, vault = context

    test_ids = [f"test-id-{i}" for i in range(10)]
    rx = b"received"
    for test_id in test_ids:
        run(["receive", test_id, "-", OCTET_STREAM], stdin=rx)

    run(["accept-all"])

    for test_id in test_ids:
        result, output = run(["accepted", test_id, "-"])
        assert result == ExitCode.OK
        assert output == rx


def test_accepted_with_no_existing_vault(context):
    run, vault = context
    result, _ = run(["accepted", "test-id-2", "-"])
    assert result == ExitCode.OS_FILE


def test_accepted_with_no_accepted(context):
    run, vault = context
    run(["receive", "test-id-1", "-", OCTET_STREAM], stdin=b"received")
    result, _ = run(["accepted", "test-id-2", "-"])
    assert result == ExitCode.DATA_ERR


def test_receive_receives_data(context):
    run, vault = context
    test_id = "test_id"
    rx = b"received"

    run(["receive", test_id, "-", OCTET_STREAM], stdin=rx)

    return_code, output = run(["received", test_id, "-"])
    assert return_code == ExitCode.OK
    assert output == rx


def test_received_with_no_existing_vault(context):
    run, vault = context
    result, _ = run(["received", "test-id-2", "-"])
    assert result == ExitCode.OS_FILE


def test_received_with_no_received(context):
    run, vault = context
    run(["receive", "test-id-1", "-", OCTET_STREAM], stdin=b"received")
    result, _ = run(["received", "test-id-2", "-"])
    assert result == ExitCode.DATA_ERR
