from godkjenn.comparators import exact, ignore_line_endings

class TestExact:
    def test_basic(self):
        assert exact(b'asdf', b'asdf')

    def test_basic_mismatch(self):
        assert not exact(b'asdf', 'asdg')

    def test_line_ending_mismatch(self):
        assert not exact(b'\r\n', b'\n')

class TestIgnoreLineEndings:
    def test_windows_vs_unix(self):
        assert ignore_line_endings(b'\r\n', b'\n')

    def test_unix_vs_windows(self):
        assert ignore_line_endings(b'\n', b'\r\n')

    def test_multiple_occurrences(self):
        assert ignore_line_endings(b'\n\n', b'\r\n\r\n')

    def test_interspersed(self):
        assert ignore_line_endings(b'foo\nbar\nbaz', b'foo\r\nbar\r\nbaz')

    def test_mismatch(self):
        assert not ignore_line_endings(b'asdf\r\n', b'asdg\n')

    def test_mismatched_line_ending_count(self):
        assert not ignore_line_endings(b'\r\n', b'\n\n')