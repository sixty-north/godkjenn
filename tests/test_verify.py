import hypothesis.strategies as ST
import pytest
from hypothesis import assume, given

from godkjenn.comparators import exact, ignore_line_endings
from godkjenn.verification import MismatchError, verify

from utils import OCTET_STREAM, temp_vault  # noqa: F401


@given(ST.binary())
def test_verify_matching_values(data):
    with temp_vault() as vault:
        vault.receive("id", data, OCTET_STREAM, None)
        vault.accept("id")
        verify(vault, exact, "id", data, OCTET_STREAM, None)


@given(ST.binary())
def test_verify_missing_accepted_raises_MismatchError(data):
    with temp_vault() as vault:
        with pytest.raises(MismatchError):
            verify(vault, exact, "id", data, OCTET_STREAM, None)


@given(ST.binary(), ST.binary())
def test_verify_mismatch_raises_MismatchError(accepted, received):
    assume(accepted != received)
    with temp_vault() as vault:
        vault.receive("id", accepted, OCTET_STREAM, None)
        vault.accept("id")
        with pytest.raises(MismatchError):
            verify(vault, exact, "id", received, OCTET_STREAM, None)


@given(ST.binary(), ST.binary())
def test_verifying_matching_result_clears_received(accepted, received):
    assume(accepted != received)
    with temp_vault() as vault:
        vault.receive("id", accepted, OCTET_STREAM, None)
        vault.accept("id")
        vault.receive("id", received, OCTET_STREAM, None)
        assert vault.received("id").path.read_bytes() == received

        verify(vault, exact, "id", accepted, OCTET_STREAM, None)

        assert vault.accepted("id").path.read_bytes() == accepted
        with pytest.raises(KeyError):
            vault.received("id")


def test_verifying_equivalent_but_different_data_leaves_accepted_data_unchanged():
    accepted_data = b'asdf\r\n'
    received_data = b'asdf\n'
    with temp_vault() as vault:
        vault.receive("id", accepted_data, OCTET_STREAM, None)
        vault.accept("id")
        assert verify(vault, ignore_line_endings, "id", received_data, OCTET_STREAM, None) is None

        accepted = vault.accepted("id")
        assert accepted.path.read_bytes() == accepted_data
        
        with pytest.raises(KeyError):
            vault.received("id")
