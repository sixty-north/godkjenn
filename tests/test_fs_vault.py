from hypothesis import given
import hypothesis.strategies as ST
import pytest

from utils import OCTET_STREAM, temp_vault  # noqa: F401


def test_missing_approved_raises_KeyError():
    with temp_vault() as vault:
        with pytest.raises(KeyError):
            vault.accepted("id")


@given(ST.binary())
def test_retrieve_accepted(accepted):
    with temp_vault() as vault:
        vault.receive("id", accepted, OCTET_STREAM, None)
        vault.accept("id")
        assert vault.accepted("id").path.read_bytes() == accepted


@given(ST.binary())
def test_replace_accepted(accepted):
    with temp_vault() as vault:
        vault.receive("id", accepted * 2, OCTET_STREAM, None)
        vault.accept("id")
        vault.receive("id", accepted, OCTET_STREAM, None)
        vault.accept("id")
        assert vault.accepted("id").path.read_bytes() == accepted


def test_missing_received_raises_KeyError():
    with temp_vault() as vault:
        with pytest.raises(KeyError):
            vault.received("id")


@given(ST.binary())
def test_retrieve_received(received):
    with temp_vault() as vault:
        vault.receive("id", received, OCTET_STREAM, None)
        assert vault.received("id").path.read_bytes() == received


@given(ST.binary())
def test_replace_received(received):
    with temp_vault() as vault:
        vault.receive("id", received * 2, OCTET_STREAM, None)
        vault.receive("id", received, OCTET_STREAM, None)
        assert vault.received("id").path.read_bytes() == received


@given(ST.binary())
def test_accept_removes_data(received):
    with temp_vault() as vault:
        vault.receive("id", received, OCTET_STREAM, None)
        vault.accept("id")
        with pytest.raises(KeyError):
            vault.received("id")


@pytest.fixture(scope="session")
def test_ids():
    return [
        "foo/bar/baz::[1]",
        "fnord/llama/yak::some_test[2]",
    ]


@given(ST.binary())
def test_iterate_received_test_ids(test_ids, artifact):
    with temp_vault() as vault:
        for test_id in test_ids:
            vault.receive(test_id, artifact, OCTET_STREAM, None)

        assert sorted(vault.ids()) == sorted(test_ids)


@given(ST.binary())
def test_iterate_accepted_test_ids(test_ids, artifact):
    with temp_vault() as vault:
        for test_id in test_ids:
            vault.receive(test_id, artifact, OCTET_STREAM, None)
            vault.accept(test_id)

        assert sorted(vault.ids()) == sorted(test_ids)


@given(ST.binary())
def test_iterate_mixed_test_ids(test_ids, artifact):
    with temp_vault() as vault:
        for test_id in test_ids:
            vault.receive(test_id, artifact, OCTET_STREAM, None)

        for test_id in test_ids[: len(test_ids) // 2]:
            vault.accept(test_id)

        assert sorted(vault.ids()) == sorted(test_ids)


@given(ST.binary())
def test_colliding_test_ids_are_avoided(artifact):
    test_ids = ("foo::bar", "foo__bar")
    with temp_vault() as vault:
        for test_id in test_ids:
            vault.receive(test_id, artifact, OCTET_STREAM, None)
            vault.accept(test_id)

        assert sorted(vault.ids()) == sorted(test_ids)


INVALID_WINDOWS_PATH_CHARS = (
    "<",
    ">",
    ":",
    '"',
    "\\",
    "|",
    "?",
    "*",
)


@pytest.fixture(params=INVALID_WINDOWS_PATH_CHARS)
def invalid_windows_path_char(request):
    return request.param


def test_test_id_with_invalid_windows_path_char(invalid_windows_path_char):
    test_id = f"test{invalid_windows_path_char}"
    with temp_vault() as vault:
        vault.receive(test_id, b"1234", OCTET_STREAM, None)
        assert vault.received(test_id).path.read_bytes() == b"1234"


def test_vault_supports_multiple_test_ids_differing_only_by_invalid_characters():
    test_ids = tuple(f"test{char}" for char in INVALID_WINDOWS_PATH_CHARS)
    with temp_vault() as vault:
        for test_id in test_ids:
            vault.receive(test_id, test_id.encode("utf-8"), OCTET_STREAM, None)

        assert sorted(test_ids) == sorted(vault.ids())

        for test_id in test_ids:
            assert vault.received(test_id).path.read_bytes() == test_id.encode("utf-8")


def test_clear_received_data_with_no_accepted_data():
    with temp_vault() as vault:
        vault.receive("test-id", b"data", OCTET_STREAM, None)
        vault.clear_received("test-id")
        with pytest.raises(KeyError):
            vault.received("test-id")


def test_clear_received_data_with_accepted_data():
    with temp_vault() as vault:
        vault.receive("test-id", b"data", OCTET_STREAM, None)
        vault.accept("test-id")
        vault.receive("test-id2", b"data", OCTET_STREAM, None)
        vault.clear_received("test-id2")
        with pytest.raises(KeyError):
            vault.received("test-id2")


def test_clear_received_with_no_received_or_accepted_data():
    with temp_vault() as vault:
        vault.clear_received("test-id")


def test_clear_received_with_no_received_data():
    with temp_vault() as vault:
        vault.receive("test-id", b"data", OCTET_STREAM, None)
        vault.accept("test-id")
        vault.clear_received("test-id2")
