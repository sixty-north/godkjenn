========
godkjenn
========

.. highlights::

    *godkjenn* 
        /go:kjen/
        Approve (Norwegian)

Approval testing for Python3.

|full_documentation|_

.. |full_documentation| replace:: **Read the full documentation at readthedocs.**
.. _full_documentation: http://godkjenn.readthedocs.org/en/latest/