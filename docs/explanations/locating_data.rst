=============
Locating data
=============

TL;DR
=====

Tell godkjenn where to start looking for the data directory with the ``-C`` option. Tell it the name of
the data directory with the ``-d`` options. E.g. ``godkjenn -C tests -d .godkjenn_dir status``.

``-C`` defaults to "." (i.e. the current directory), and ``-d`` defaults to ".godkjenn".

Overview
========

Godkjenn stores data for each call to ``verify()`` in your tests. The data it stores includes any accepted or received
data for the test, as well as any metadata required for the other data (e.g. mime types, encoding, etc.). Godkjenn wraps all of
this up in the concept of a *vault*, and the vault stores this data on the filesystem.

When you run godkjenn, it needs to know where to find the vault data. It finds this based on two pieces of data: a
starting directory and the name of the *data directory*. By default the starting directory is whichever directory you
run godkjenn from. Similarly, the default value for the *data directory* name is ".godkjenn". Godkjenn starts by looking
for the *data directory* name in the starting directory. If it doesn't find it, it checks the parent of the starting
directory. It continues looking up the ancestor directories until either a) it find a directory containing the data
directory or b) it runs out of ancestors.

Assuming a data directory is found, godkjenn now has the information it needs to run.

An example
==========

Suppose you had a directory structure like this:

.. code-block::

    my_project/
        tests/
            .godkjenn

Here the ".godkjenn" directory is the *data directory* that godkjenn needs to find.

The simplest mode for godkjenn is if you run it from the 'tests' directory:

.. code-block::

    cd my_project/tests
    godkjenn status

Run this way, godkjenn will find the ".godkjenn" directory using its default settings. It will first start its search
from the "tests" directory because that's where we're running godkjenn from. Since by default it looks for a directory
called ".godkjenn", it will find it immediately.

Specifying a start directory
----------------------------

Suppose though that you wanted to start godkjenn from another directory. If we wanted to start godkjenn from the
'my_project' directory we'd need to tell it where to start looking for ".godkjenn". We can use the ``-C`` command line
argument to do this. Here's how it looks if we start godkjenn from the 'my_project' directory:

.. code-block::

    cd my_project
    godkjenn -C tests status

In this case, instead of starting the search in the "my_project" directory as it would by default, godkjenn starts the
search from the 'tests' directory.

Using a different data directory name
-------------------------------------

While unusual, it is technically possible to use a different name for the data directory. Suppose you instead had this
directory structure:

.. code-block::

    my_project/
        tests/
            .godkjenn_data

You can tell godkjenn to look for a different name with the ``-d`` command line option. For example, to run godkjenn from
the 'my_project' directory with this structure you'd use this:

.. code-block::

    cd my_project
    godkjenn -C tests -d .godkjenn_data status

Again, you won't normally need this, but it's there if you do.

Creating the data directory
===========================

Probably the most common way to create the data directory is to let godkjenn do it for you automatically. This generally
happens in one of two ways. First, when you run tests using the pytest integration, it will create a ".godkjenn"
directory in the pytest root directory.  `Pytest uses a fairly involved algorithm to determine the location of the root
directory <https://docs.pytest.org/en/stable/customize.html#finding-the-rootdir>`_.

Another way to create the data directory is with the ``pytest receive`` command. If no existing data directory exists
(as determined by the algorithm described above), then this command will try to create a ".godkjenn" directory in your
current directory. If you use the ``-C`` and/or ``-d`` arguments to change where godkjenn starts its search, then
``godkjenn receive`` will create the directory there instead.

If you **don't** want ``godkjenn receive`` to create a directory, you can pass the "--no-init" argument.