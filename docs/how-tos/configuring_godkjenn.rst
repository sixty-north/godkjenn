====================
Configuring godkjenn
====================

Godkjenn support some minor configuration through the file ``config.toml`` in the
data directory. This file must contain a top-level 'godkjenn' key, for example:

.. code-block:: toml

    [godkjenn]

Everything under this key is part of the godkjenn configuration.

Configurable options
====================

differs.default_command
-----------------------

The ``godkjenn.differs.default_command`` option specifies the default tool to use for displaying diffs. The value is a
template used with the ``str.format()`` method that is passed ``Artifact`` instances for the received and accepted data.
The call looks like this:

.. code-block:: python

    command_template = . . . value of differs.default_command . . .
    command = command_template.format(accepted=accepted_artifact, received=received.artifact)

As you can see, the artifacts are passed using the 'accepted' and 'received' keyword arguments.

A common template value would extract the ``path`` attribute from each artifacts and use those as arguments to a
diff/merge tool. For example, here's a config that passes the artifact paths to `vimdiff
<http://vimdoc.sourceforge.net/htmldoc/diff.html#vimdiff>`_:

.. code-block:: toml

    [godkjenn.differs]
    default_command = "vimdiff {accepted.path} {received.path}"

This option is only used if there is not a MIME-type-specific tool defined in ``godkjenn.differs.mime_types``.

differs.mime_types
------------------

The ``godkjenn.differs.mime_types`` option allows you to specify diff commands for specific MIME-types. It is a mapping
from MIME-types to command template (as described in the ``differs.default_command`` option). When godkjenn needs to run
a diff tool for an artifact the MIME-type of the *received* data is looked up in the ``differs.mime_types`` mapping, and
if it's found then the value is used as the command template. If no match is found, then ``differs.default_command`` is
used as the default.

For example, here's how you could specify that the ``image-diff`` tool should be used for PNGs, ``vimdiff`` should be
used for plain text, and ``bcomp`` for everything else:

.. code-block:: toml

    [godkjenn.differs]
    default_command = "bcomp {accepted.path} {received.path}"

    [godkjenn.differs.mime_types]
    "image/png" = "image-diff {accepted.path} {received.path}"
    "text/plain" = "vimdiff {accepted.path} {received.path}"

.. note::

    If there is no ``differs.mime_types`` entry for an artifact, and if ``differs.default_command`` is not set, godkjenn
    fallsback to some fairly primitive built-in diffing tools. You're almost always best off configuring at least the
    ``differs.default_command`` options.