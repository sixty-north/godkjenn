Godkjenn: approval testing for Python 3
=======================================

.. highlights::

    *godkjenn* 
        /go:kjen/
        Approve (Norwegian)

Approval testing for Python 3.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   introduction
   tutorial/index
   how-tos/configuring_godkjenn
   explanations/locating_data



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
